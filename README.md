# Sendmail
Sendmail.sh is a stupidly simple shell script for
sending mails from your terminal

![](scrot.png)

### How do I use it?
The script will first ask you for the receivers address, the subject of 
the mail and for a path to a possible attachment that you may want to include (leave
blank if you don't want to include an attachment.) 

Next it will open up vim for you 
to type the body of the mail into. 

Save and quit out of vim and if everything was succesfull, you will see 'Mail sent succesfully'

### Depencies
Sendmail.sh depends on the 's-nail' package, use your favourite package manager 
to install it, for example

```
sudo pacman -S s-nail
```

### Reminders:
* Remember to configure your .mailrc first
* sendmail.sh can only send one attachment at a time :(
