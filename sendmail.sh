#!/bin/sh

# Get the receiver, subject and possible attachment
read -r -p "To: " to
read -r -p "Subject: " sub
read -r -p "Attachment: " att

# Open an empty file with vim
vim /tmp/mail

# Check if an attachment was included 
if [ -z "$att" ]
then
    # Send the mail without an attachment and hide the output 
    mail -v -s "$sub" "$to" < /tmp/mail &> /dev/null || exit
else
    # Send the mail with the attachment and hide the output 
    mail -a "$att" -v -s "$sub" "$to" < /tmp/mail &> /dev/null || exit 
fi

# Finally remove the message file if the mail was sent
rm /tmp/mail
echo "Mail sent succesfully"
